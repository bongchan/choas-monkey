package com.skcc.chaos.portal.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class RestAPIController {
	
	@Value("${monkey.url}")
	private String url;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@RequestMapping(value = "schedule/{id}", method = RequestMethod.POST)
	public String updateSchdule(@PathVariable String id, @RequestBody String expression) {
		System.out.println("id = " + id);
		System.out.println("expression + " + expression);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		HttpEntity entity = new HttpEntity(expression, headers);
		
		
		 ClientHttpRequestFactory httpRequestFactory =  new HttpComponentsClientHttpRequestFactory();
		 restTemplate = new RestTemplate(httpRequestFactory);
		
		ResponseEntity<String> exchange = restTemplate.exchange(url + "/schedules/" + id, HttpMethod.PATCH, entity, String.class);
		System.out.print(exchange.getBody());
		return exchange.getBody();
	}
	
}
