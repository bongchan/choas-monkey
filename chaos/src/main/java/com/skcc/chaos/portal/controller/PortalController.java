package com.skcc.chaos.portal.controller;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.skcc.chaos.portal.service.PortalService;

@Controller
public class PortalController {
	
	@Autowired
	PortalService service;
	
	@RequestMapping(value = "/event", method = RequestMethod.GET)
	public String events(Model model) throws JSONException {
		ArrayList<HashMap<String, String>> events = service.getEvents();
		
		model.addAttribute("data", events);
		
		return "event";
	}
	
//	@RequestMapping(value = "/event", method = RequestMethod.GET)
//	public String schedules(Model model) throws JSONException {
//		ArrayList<HashMap<String, String>> events = service.getSchedules();
//		
//		model.addAttribute("data", events);
//		
//		return "event";
//	}
	
	@RequestMapping(value = "/chaos", method = RequestMethod.GET)
	public String chaos(Model model) {
		
		ArrayList<HashMap<String, String>> chaoses = null;
		try {
			chaoses = service.getChaoses();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("data", chaoses);
		
		return "chaos";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		
		ArrayList<HashMap<String, String>> chaoses = null;
		try {
			chaoses = service.getChaoses();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("data", chaoses);
		
		return "chaos";
	}
}