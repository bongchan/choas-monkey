package com.skcc.chaos.portal.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("portalService")
public class PortalService {
	
	@Value("${monkey.url}")
	private String url;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public JSONObject getApplications() throws JSONException {
		System.out.println("PortalService: getApplications : url  = " + url + "/applications");
		
		String data = restTemplate.getForObject(url + "/applications", String.class);
		JSONObject jsonData = new JSONObject(data);
		
		System.out.println(jsonData);
		
		return jsonData;
	}
	
	public ArrayList<HashMap<String, String>> getEvents() throws JSONException {
		System.out.println("PortalService: getEvents : url  = " + url + "/events");
		
		String data = restTemplate.getForObject(url + "/events?size=20", String.class);
		int totalPages = new JSONObject(data).getJSONObject("page").getInt("totalPages");
		
		
		data = restTemplate.getForObject(url + "/events?page=" + (totalPages - 1) + "&size=20", String.class);
		org.json.JSONArray events = new JSONObject(data).getJSONObject("_embedded").getJSONArray("events");
		
		
		ArrayList<HashMap<String, String>> result = new ArrayList<>();
		HashMap<String, String> eventMap;
		JSONObject event;
		JSONObject links;
		
		for(int i = 0 ; i < events.length(); i++) {
			eventMap = new HashMap<>();
			event = events.getJSONObject(i);
			links = event.getJSONObject("_links");
			
			eventMap.put("executedAt", event.getString("executedAt"));
			eventMap.put("terminatedInstances", event.getJSONArray("terminatedInstances").toString());
			eventMap.put("totalInstanceCount", String.valueOf(event.getInt("totalInstanceCount")));
			eventMap.put("terminatedInstanceCount", String.valueOf(event.getInt("terminatedInstanceCount")));
			eventMap.put("chaosId", links.getJSONObject("chaos").getString("href").replace(url + "/chaoses/", ""));
			
			result.add(eventMap);
		}
		
		return result;
	}
	
	public org.json.JSONArray getSchedules() throws JSONException {
		System.out.println("PortalService: getScheduloe : url  = " + url + "/schedules");
		
		String data = restTemplate.getForObject(url + "/schedules", String.class);
		JSONObject jsonData = new JSONObject(data);
		
		org.json.JSONArray schdules = jsonData.getJSONObject("_embedded").getJSONArray("schedules");
		
		System.out.println(jsonData);
		
		return schdules;
	}
	
	public ArrayList<HashMap<String, String>> getChaoses() throws JSONException {
		System.out.println("PortalService: getChaoses : url  = " + url + "/chaoses");
		
//		String data = restTemplate.getForObject(url + "/chaoses", String.class);
//		JSONObject jsonData = new JSONObject(data);
		JSONObject jsonData = new JSONObject("{\"_embedded\":{\"chaoses\":[{\"probability\":0.1,\"_links\":{\"self\":{\"href\":\"https://chaos-lemur/chaoses/34\"},\"application\":{\"href\":\"https://chaos-lemur/applications/60\"},\"schedule\":{\"href\":\"https://chaos-lemur/schedules/31\"}}},{\"probability\":0.1,\"_links\":{\"self\":{\"href\":\"https://chaos-lemur/chaoses/35\"},\"application\":{\"href\":\"https://chaos-lemur/applications/61\"},\"schedule\":{\"href\":\"https://chaos-lemur/schedules/31\"}}},{\"probability\":0.1,\"_links\":{\"self\":{\"href\":\"https://chaos-lemur/chaoses/36\"},\"application\":{\"href\":\"https://chaos-lemur/applications/62\"},\"schedule\":{\"href\":\"https://chaos-lemur/schedules/31\"}}}]},\"_links\":{\"first\":{\"href\":\"https://chaos-lemur/chaoses?page=0&size=3\"},\"prev\":{\"href\":\"https://chaos-lemur/chaoses?page=1&size=3\"},\"self\":{\"href\":\"https://chaos-lemur/chaoses\"},\"next\":{\"href\":\"https://chaos-lemur/chaoses?page=3&size=3\"},\"last\":{\"href\":\"https://chaos-lemur/chaoses?page=3&size=3\"}},\"page\":{\"size\":3,\"totalElements\":12,\"totalPages\":4,\"number\":2}}");
		org.json.JSONArray chaoses = jsonData.getJSONObject("_embedded").getJSONArray("chaoses");
		
		ArrayList<HashMap<String, String>> result = new ArrayList<>();
		HashMap<String, String> chaosMap;
		JSONObject chaos;
		String appurl;
		String scheduelurl;
		String expression;
		String appid;
		
		for(int i = 0 ; i < chaoses.length(); i++) {
			chaos = chaoses.getJSONObject(i).getJSONObject("_links");
			chaosMap = new HashMap<>();
			scheduelurl = chaos.getJSONObject("schedule").getString("href");
			appurl = chaos.getJSONObject("application").getString("href");
			
//			expression = new JSONObject(restTemplate.getForObject(scheduelurl, String.class)).getString("expression");
//			appid = new JSONObject(restTemplate.getForObject(appurl, String.class)).getString("applicationId");
			
			chaosMap.put("chaosId", chaos.getJSONObject("self").getString("href").replace(url + "/chaoses/", ""));
			chaosMap.put("applicationId", appurl.replace(url + "/applications/", ""));
			chaosMap.put("scheduleId", scheduelurl.replace(url + "/schedules/", ""));
//			chaosMap.put("expression", expression);
//			chaosMap.put("appid", appid);
			
			result.add(chaosMap);
		}
		
		System.out.println(result.toString());
		
		return result;
	}
}
