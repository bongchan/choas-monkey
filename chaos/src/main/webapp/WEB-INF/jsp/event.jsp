<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="java.util.*"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Overview</title>

<!-- Bootstrap -->
<link href="<c:url value='/resources/vendors/bootstrap/css/bootstrap.css'/>" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<c:url value='/resources/app/css/custom.css'/>" rel="stylesheet">
</head>

<body>
	<%
		ArrayList<HashMap<String, String>> data = (ArrayList<HashMap<String, String>>) request.getAttribute("data");
	%>
	<nav class="navbar navbar-default nav" style="padding-left: 100px;">
		<div class="container-fluid">
			<div class="navbar-header">
				<p class="navbar-text navbar-right">
					<img src="<c:url value='/resources/app/images/chaos.png'/>" class="img-rounded listImage">
					<a href="#" class="navbar-link navTitle">Chaos Monkey</a>
				</p>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="subTitle">Events</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="list-group">
					<a href="https://chaos-portal.sk.kr.mybluemix.net/chaos" class="list-group-item">chaoses</a> <a href="https://chaos-portal.sk.kr.mybluemix.net/event" class="list-group-item active">Events</a>
				</div>
			</div>
			<div class="col-md-8">
				<div class="list-group">

					<c:forEach var="chaos" items="${data}">
						<div class="list-group-item listRow">
							<div class="floatLeft">
								<h4 class="list-group-item-heading">Chaos-${chaos.chaosId}</h4>
								<p class="list-group-item-text">
									Execute time: ${chaos.executedAt}<br /> 
									TerminatedInstances: ${chaos.terminatedInstances} <br />
									TotalInstanceCount: ${chaos.totalInstanceCount} <br />
									TerminatedInstanceCount: ${chaos.terminatedInstanceCount}								
								</p>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<c:url value='/resources/vendors/jquery/jquery.min.js'/>"></script>
	<!-- Bootstrap -->
	<script src="<c:url value='/resources/vendors/bootstrap/js/bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/app/js/dashboard.js'/>"></script>
</body>
</html>