$(document).ready(function(){
	window.saveBtnHandler = function(scheduleID) {
		var data = {};
		
		data.expression = $('#input_' + scheduleID).val();
		$.ajax({
		    url : 'api/schedule/' + scheduleID,
		    data : JSON.stringify(data),
		    type : 'POST',
		    contentType : 'application/json',
		    success: function(data) {
		    	alert("설정이 저장되었습니다.");
		    	location.reload();
		    }, 
		    error : function(xhr, status, error) {
                console.log(xhr);
                console.log(status);
                console.log(status);
          }
		});
	}
});